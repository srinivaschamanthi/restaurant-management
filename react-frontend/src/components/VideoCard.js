import React from 'react'
import './VideoCard.css'
import Avatar from "@material-ui/core/Avatar"


function VideoCard(props) {

        const {each} = props
        const {
          name,
          managerName,
          timings,
          phonenumber,
          menu
        } = each
    

    return (
        <div className='videoCard' >
             
            <div className='VideoCard__info'>
              
                <Avatar 
                className='videoCard__avatar'
                src="https://image.similarpng.com/very-thumbnail/2021/07/Chef-restaurant-logo-illustrations-template-on-transparent-background-PNG.png"
                alt={name}/>
                <div className='video__text'>
                    <h4>{name}--Restaurant</h4>
                    <p>{managerName}--Manager</p>
                    <p>{phonenumber}</p>
                    <p>{timings}</p>
                    <h3>MENU</h3>
                    <p>1.{menu[0]}</p>
                    <p>2.{menu[1]}</p> 
                    <p>3.{menu[2]}</p> 
                    <p>4.{menu[3]}</p> 
                    <p>5.{menu[4]}</p> 




                        
                        
                    

                </div>
            </div>
        </div>
    
    )
}

export default VideoCard
