import { Restaurant_data } from "../Utils/MongoDB.js";


const restaurant_details = (request, response) => {
    Restaurant_data.find({})
      .then((data) => {
        response.send(data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  

  export {restaurant_details};
  