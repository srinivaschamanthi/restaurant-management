import { Component } from "react";

class Updaterestaurant extends Component {
  state = {
    phonenumber: "",
    managerName: "",
  };

  changePhoneNumber = (event) => {
    this.setState({ phonenumber: event.target.value });
  };

  changeManagerName = (event) => {
    this.setState({ managerName: event.target.value });
  };

  submitingUpdatedData = async (event) => {
    event.preventDefault();
    const url = "http://localhost:8000/updateres";
    const { phonenumber, managerName } = this.state;
    const bodyData = {
      phonenumber,
      managerName,
    };
    const option = {
      method: "PUT",
      body: JSON.stringify(bodyData),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, option);
    const data = await response.json();
    if (data.status_code === 200) {
      this.setState({
        phonenumber: "",
        managerName: "",
      });
      alert("data updated successfully");
    }
    else{
        console.log(data);
    }
  };

  render() {
    return (
      <div className="form animated flipInX">
        <h2>Update Details</h2>
        <form onSubmit={this.submitingUpdatedData}>
            <p>Enter your Registered Number to update Manager Name</p>
          <input placeholder="phoneNumber" type="text" onChange={this.changePhoneNumber} />
          <input placeholder="Manager Name" type="text" onChange={this.changeManagerName} />
          <button type="submit">Update</button>
        </form>
      </div>
    );
  }
}

export default Updaterestaurant;
