import { Restaurant_data } from "../Utils/MongoDB.js";

const delete_restaurant_handler = (request,response)=>{
    const phonenumber = request.body.phonenumber;

    async function run() {
      
        await Restaurant_data.deleteOne({ phonenumber: phonenumber });
      
      }
      

      response.send(JSON.stringify("Restaurant Deleted successfully"));
      run();

}

// const delete_restaurant = (phonenumber) => {

//     Restaurant_data.findOneAndRemove({ phonenumber: phonenumber }, (err) => {
  
//       if (err) {
  
//         console.log(err);
  
//       }else{
     
//         console.log("deleted successfully");   
//     }
  
//     });
  
//   };

export {delete_restaurant_handler}
