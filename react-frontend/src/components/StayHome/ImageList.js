import * as React from "react";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";

export function Veg() {
  return (
    <ImageList
      sx={{ width: 500, height: 200 }}
      variant="woven"
      cols={3}
      gap={8}
    >
      {itemData.map((item) => (
        <ImageListItem key={item.img}>
          <img
            src={`${item.img}?w=161&fit=crop&auto=format`}
            srcSet={`${item.img}?w=161&fit=crop&auto=format&dpr=2 2x`}
            alt={item.title}
            loading="lazy"
          />
        </ImageListItem>
      ))}
    </ImageList>
  );
}

const itemData = [
  {
    img: "https://www.lytmeals.in/storage/uploads/images/164433293746.jpeg",
    title: "veg combo",
  },
  {
    img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTPtCD7bZQapJwP0Q-FQ0hgwSpagcKnrlIVhg&usqp=CAU",
    title: "Kitchen",
  },
  {
    img: "https://mixthatdrink.com/wp-content/uploads/2020/02/sex-in-the-driveway-drink-3.jpg",
    title: "Sink",
  },
];

export default function NonVeg() {
  return (
    <ImageList
      sx={{ width: 500, height: 200 }}
      variant="woven"
      cols={3}
      gap={8}
    >
      {nonVegData.map((item) => (
        <ImageListItem key={item.img}>
          <img
            src={`${item.img}?w=161&fit=crop&auto=format`}
            srcSet={`${item.img}?w=161&fit=crop&auto=format&dpr=2 2x`}
            alt={item.title}
            loading="lazy"
          />
        </ImageListItem>
      ))}
    </ImageList>
  );
}
const nonVegData = [
  {
    img: "https://travelmagnet.in/wp-content/uploads/2021/07/128895303-assorted-indian-non-vegetarian-food-recipe-served-in-a-group-includes-chicken-curry-mutton-masala-an.jpg",
    title: "nonveg combo",
  },
  {
    img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTt8X7UMXhNrwx0yzO77_lirSLF5Qak_4NB0Q&usqp=CAU",
    title: "Kitchen",
  },
  {
    img: "https://www.acouplecooks.com/wp-content/uploads/2021/02/Painkiller-Cocktail-008.jpg",
    title: "Sink",
  },
];
export function Deserts() {
  return (
    <ImageList
      sx={{ width: 500, height: 200 }}
      variant="woven"
      cols={3}
      gap={8}
    >
      {desertData.map((item) => (
        <ImageListItem key={item.img}>
          <img
            src={`${item.img}?w=161&fit=crop&auto=format`}
            srcSet={`${item.img}?w=161&fit=crop&auto=format&dpr=2 2x`}
            alt={item.title}
            loading="lazy"
          />
        </ImageListItem>
      ))}
    </ImageList>
  );
}
const desertData = [
  {
    img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRDeN1u6a07ZJ6y2ZjizqcJAEyx2LA_8-fpPQ&usqp=CAU",
    title: "nonveg combo",
  },
  {
    img: "https://insanelygoodrecipes.com/wp-content/uploads/2021/01/White-Chocolate-Oreo-Cookie-Balls.png",
    title: "Kitchen",
  },
  {
    img: "https://images.immediate.co.uk/production/volatile/sites/2/2017/12/xmas-Cover-17v5-54a9395.jpg?quality=45&resize=768,574",
    title: "Sink",
  },
 
];
