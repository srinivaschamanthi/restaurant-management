import React, { Component } from 'react'
import './HomeRestaurants.css'
import VideoCard from '../VideoCard';

class HomeRestaurants extends Component {
    state = {
        RestaurantsData :[{name:"Hello Cafe",
            managerName:"Srinivas",
            timings:"11AM TO 11PM",
            phonenumber:9133669797,
            menu:["Plain Cheese Pizza","Tandoori Paneer Tikka","Soya Tandoori Tikka ","Tawa Parantha","Pudina Parantha"]}]
    }

    componentDidMount(){
        this.getServerData()
    }

    getServerData = async()=>{
        const url = "http://localhost:8000/home"
        const option = {
            method:"GET",
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json"
            }
        }
        const response = await fetch(url,option)
        console.log(response)
        const data = await response.json()
        // const{RestaurantsData} = this.state
        this.setState({
            RestaurantsData:[...this.state.RestaurantsData,...data]
        })
        console.log(this.state.RestaurantsData)
    }

    render(){
        const {RestaurantsData} = this.state
    RestaurantsData.sort(()=>Math.random()-0.5)
    return (
        <div className='homevideos'>
            <h2>Recommended Restaurants</h2> 
           <div className='homeVideos_videos'>
          {RestaurantsData.map(each => (
            <VideoCard each={each} key={each.id} />
          ))}

           </div>
        </div>
    )
    }
}

export default HomeRestaurants
