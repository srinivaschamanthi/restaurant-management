import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";

export default function ActionAreaCard() {
  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardActionArea>
        <CardMedia
          component="img"
          height="140"
          image="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSZl-tu4GhT-NoqcY8qYH32DTt9POffPfNhkQ&usqp=CAU"
          alt="green iguana"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            Food And Bewarage
          </Typography>
          <Typography variant="body2" color="text.secondary">
            Reducing your risk of developing and dying of heart disease.
            Possibly reducing your risk of ischemic stroke (when the arteries to
            your brain become narrowed or blocked, causing severely reduced
            blood flow) Possibly reducing your risk of diabetes.
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
