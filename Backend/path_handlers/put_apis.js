import { Restaurant_data } from "../Utils/MongoDB.js";


const update_restaurant_handler = (request,response)=>{
    const managerName = request.body.managerName;
    const phonenumber = request.body.phonenumber;

    const update = async () => {
        try {
          const result = await Restaurant_data.updateOne(
            ({ phonenumber: `${phonenumber}`}),
            {
              $set: {
                managerName: `${managerName}`,
              },
            }
          );
          console.log(result);
          response.send(JSON.stringify("managerName updated successfully"));
        } catch (err) {
          console.log(err);
        }
      };
      Restaurant_data.find({ phonenumber: `${phonenumber}` }, (error, data) => {
        if (error) {
          console.log(error);
        } else {
          if (data.length !== 0) {
            update(phonenumber);
          }
        }
      });
}

export { update_restaurant_handler };
