import { Restaurant_data } from "../Utils/MongoDB.js";


const restaurants_handler = (request, response) => {
  
    const name = request.body.name;
    const managerName = request.body.managerName;
    const phonenumber = request.body.phonenumber;
    const timings = request.body.timings;
    const menu = request.body.menu;
    
    
  
    const post_data = {
      name: name,
      managerName: managerName,
      phonenumber: phonenumber,
      timings: timings,
      menu: menu,
     
    };
  
    const posts_data = new Restaurant_data(post_data);
    posts_data.save().then(() => response.send(JSON.stringify({
      status_code:200,
      status_message:"DATA Added Successfully"
    })));
  };
  

  //exporting the functions
export { restaurants_handler };
