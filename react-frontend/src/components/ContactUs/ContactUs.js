import * as React from 'react';
import Box from '@mui/material/Box';
import Rating from '@mui/material/Rating';
import Typography from '@mui/material/Typography';

export function Contactus(props) {
    
    const [value, setValue] = React.useState(3);
    return (
        <Box
        sx={{
          '& > legend': { mt: 3 },
        }}
      >
        <Typography component="legend">Rate your experince using food network app so far</Typography>
        <Rating
          name="simple-controlled"
          value={value}
          onChange={(event, newValue) => {
            setValue(newValue);
          }}
        />
        
      </Box>
    )
}
