import React from 'react';
import './Header.css';
import MenuIcon from "@material-ui/icons/Menu";
import AppsIcon from "@material-ui/icons/Apps";
import NotificationsIcon from "@material-ui/icons/Notifications";
import Avatar from "@material-ui/core/Avatar";
import { Link } from 'react-router-dom';



export function Header() {

    return (
        <div className="header">
            <div className='header_left'>
            <MenuIcon style={{marginBottom:"15px"}}/>
            <Link to='/'>
            <img className="header__logo" src="https://media.istockphoto.com/vectors/restaurant-food-drinks-logo-fork-knife-background-vector-image-vector-id981368726?k=20&m=981368726&s=612x612&w=0&h=Um4YOExWlUgOfpUs2spnN0NrrXs-M71OUuUMbStVFNQ=" alt="restaurant"/>
            </Link>
            </div>

            <div className='header_input'>
                  <h1>Food Network</h1>       
            </div>
            <div>
            </div>
            <div className='header_right'>
            <AppsIcon className='header_icon'/>   
            <NotificationsIcon className='header_icon'/> 
            <Avatar alt="Srinivas Chamanthi" src="https://res.cloudinary.com/duwpvypoh/image/upload/v1613627292/2019_07_15_18_35_IMG_0243_wol3al.jpg"/>
            </div>
            
        </div>
    )
}
