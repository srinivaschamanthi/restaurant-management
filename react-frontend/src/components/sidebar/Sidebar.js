import React from 'react'
import './Sidebar.css'
import  {SidebarRow}  from '../SidebarRow/SidebarRow'
import HomeIcon from "@material-ui/icons/Home";
import CallIcon from '@material-ui/icons/Call';
import AddBoxIcon from '@material-ui/icons/AddBox';
import CampaignIcon from '@mui/icons-material/Campaign';
import ExpandMoreOutlinedIcon from '@material-ui/icons/ExpandLessOutlined'
import WineBarIcon from '@mui/icons-material/WineBar';
import EditIcon from '@mui/icons-material/Edit';
import FastfoodIcon from '@mui/icons-material/Fastfood';
import DeleteIcon from '@mui/icons-material/Delete';
import DeckIcon from '@mui/icons-material/Deck';
import { Link } from 'react-router-dom';


export function Sidebar() {
    

    return (
        <div className='sidebar'>
            <Link to='/' style={{textDecoration: 'none'}}>
            <SidebarRow Icon={HomeIcon} title='Home'/>
            </Link>
            <Link to='/add' style={{textDecoration:'none'}}> 
            <SidebarRow Icon={AddBoxIcon} title='Add Restaurants'/>
            </Link>
            <Link to='/update' style={{textDecoration: 'none'}}>
            <SidebarRow Icon={EditIcon} title='Edit Restaurant'/>
            </Link>
            <Link to='/delete' style={{textDecoration: 'none'}}>
            <SidebarRow Icon={DeleteIcon} title='Delete Restaurant'/>
            </Link>
            <Link to='/offers' style={{textDecoration:'none'}}> 

            <SidebarRow Icon ={CampaignIcon} title='Offers'/>
            </Link>
            <hr/>
            <Link to='/snacks' style={{textDecoration:'none'}}> 

            <SidebarRow Icon ={FastfoodIcon} title='Snacks'/>
            </Link>

            <Link to='/bewarages' style={{textDecoration:'none'}}> 
            <SidebarRow Icon ={WineBarIcon} title='Bewarages'/>
            </Link>

            <Link to='/stayHome' style={{textDecoration:'none'}}> 
            <SidebarRow Icon ={DeckIcon} title='Stay Home Specials'/>
            </Link>

            <Link to='/contact' style={{textDecoration:'none'}}>
            <SidebarRow Icon ={CallIcon} title='Contact Us'/>
            </Link>
            
            <SidebarRow Icon ={ExpandMoreOutlinedIcon} title='Show more'/>
            <hr />
           


        </div>
    )
}
