
import { Component } from "react";
import './Deleterestaurant.css'
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';

class Deleterestaurant extends Component {
  state = {
        phonenumber: "",
      };
      changePhonenumber = (event) => {
        this.setState({ phonenumber: event.target.value });
      };
      submitingUserData = async (event) => {
        event.preventDefault();
        const url = "http://localhost:8000/delete";
        const { phonenumber } = this.state;
        const bodyData = {
          phonenumber,
        };
        const option = {
          method: "DELETE",
          body: JSON.stringify(bodyData),
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
          },
        };
        const response = await fetch(url, option);
        const data = await response.json();
        console.log(data);
        if (data.status_code === 200) {
          this.setState({
            phonenumber: "",
          });
          alert("data deleted successfully");
        }
      };

  render(){
  return (
    <Box
      component="form"
      sx={{
        '& .MuiTextField-root': { m: 1, width: '25ch' },
      }}
      noValidate
      autoComplete="off"
    >
      <div className="delContainer">
        <h3>Enter your registered mobile number to delete restaurant</h3>
        <TextField
          required
          id="outlined-required"
          label="Registered Number"
          onChange={this.changePhonenumber}
        />
      <Button  variant="outlined" onClick={this.submitingUserData}>Delete Restaurant</Button>

      </div>

    </Box>
  );
}
}

export default Deleterestaurant;