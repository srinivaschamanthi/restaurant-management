import React, { Component } from 'react'
import './Addrestaurants.css'

export default class Addrestaurants extends Component {
    state = {
    name: "",
    managerName: "",
    phonenumber: "",
    timings:"",
    menu: [],
    is_data: false,
      };

      changeName = (event) => {
        this.setState({ name: event.target.value });
      };
      changeManagerName = (event) => {
        this.setState({ managerName: event.target.value });
      };

      changeTimings = (event) => {
        this.setState({ timings: event.target.value });
      };
      changeMenu = (event) => {
        this.setState({ menu: event.target.value });
      };
      changePhoneNumber = (event) => {
        this.setState({ phonenumber: event.target.value });
      };


      submitingUserData = async (event) => {
        event.preventDefault();
        const url = "http://localhost:8000/addres";
        const { name, phonenumber, managerName, timings, menu } =
          this.state;
        const bodyData = {
          name,
          managerName,
          phonenumber,
          timings,
          menu,
        };
        const option = {
          method: "POST",
          body: JSON.stringify(bodyData),
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
          },
        };
        const response = await fetch(url, option);
        const data = await response.json();
        if (data.status_code === 200) {
          this.setState({
            name: "",
            description: "",
            merchantname: "",
            discount: "",
            price: "",
            quantity: "",
            err_msg:"",
            is_data: false
          });
          alert("data added successfully");
        } else {
          this.setState({ err_msg: data.status_message, is_data: true });
        }
      };

    render() {
    const { is_data, err_msg } = this.state;

        return (
            <>
                <div className="form animated flipInX">
        <h2>Add Restaurants</h2>
        <form onSubmit={this.submitingUserData}>
          <input
            placeholder="Restaurant Name"
            type="text"
            onChange={this.changeName}
          />
          <input
            placeholder="Manager Name"
            type="text"
            onChange={this.changeManagerName}
          />
          <input placeholder="Timings" type="text" onChange={this.changeTimings} />
          <input
            placeholder="Menu"
            type="text"
            onChange={this.changeMenu}
          />
          <input
            placeholder="phone number"
            type="text"
            onChange={this.changePhoneNumber}
          />
          <button type="submit">Create</button>
          {is_data && <p className="para-1">* {err_msg}</p>}
        </form>
      </div>
            </>
        )
    }
}
