import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import mongoose from "mongoose";
import dotenv from "dotenv";
dotenv.config();


//Creating an instance of express
const app = express();

//Returns middleware that only parses json
app.use(bodyParser.json());
//Returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors({ origin: "*" }));




import { restaurants_handler } from "./path_handlers/post_apis.js";
import {restaurant_details } from "./path_handlers/get_apis.js";
import {update_restaurant_handler} from "./path_handlers/put_apis.js"
import {delete_restaurant_handler} from "./path_handlers/delete_apis.js"

/****   API CALLS   ****/


app.get("/home",restaurant_details)
app.post("/addres",restaurants_handler)
app.put("/updateres",update_restaurant_handler)
app.delete("/delete",delete_restaurant_handler)

/**------------------**/


mongoose.connect(process.env.MONGOURL)
  .then(() => {
    console.log("Database connected!");
    app.listen(process.env.BACKENDPORT, () => {
      console.log(`server is running at port  ${process.env.BACKENDPORT}!`);
    })
  })
  .catch(() => {
    console.log("Unable to connect to database");
  })
