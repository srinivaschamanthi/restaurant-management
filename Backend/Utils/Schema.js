// importing mangoose from mangoose
import mongoose from "mongoose";


// Resturant schema

const Restaurant_schema = new mongoose.Schema({
    name: { type: String, required: true },
    managerName: {type: String, required: true  },
    phonenumber: { type: Number, required: true, unique: true },
    timings:{type:String,required:true},
    menu: Array
  });


  export { Restaurant_schema};
  