import './App.css';
import { Header } from './components/Header/Header';
import { Sidebar } from './components/sidebar/Sidebar';
import Addrestaurants from './components/Addrestaurants/Addrestaurants'
import HomeRestaurants from './components/HomeRestaurants/HomeRestaurants'
import {BrowserRouter as  Router,Switch,Route} from "react-router-dom";
import Updaterestaurant from './components/Updaterestaurants/Updaterestaurants';
import Deleterestaurant from './components/Deleterestaurant/Deleterestaurant';
import Offers from './components/Offers/Offers';
import  Snacks  from './components/Snacks/Snacks';
import { Contactus } from './components/ContactUs/ContactUs';
import StayHome from './components/StayHome/StayHome';
import { Bewarages } from './components/Bewarages/Bewarages';


function App() {
  return (
    <div className="app">
      <Router>
        <Header/>

        <Switch>
        <Route exact path='/'>
            <div className='app_page'>
              <Sidebar/>
              <HomeRestaurants/>
            </div>
          </Route>
          <Route exact path='/add'>
            <div className='app_page'>
              <Sidebar/>
              <Addrestaurants/>
            </div>
          </Route>
          <Route exact path='/update'>
            <div className='app_page'>
              <Sidebar/>
              <Updaterestaurant/>
            </div>
          </Route>
          <Route exact path='/delete'>
            <div className='app_page'>
              <Sidebar/>
              <Deleterestaurant/>
            </div>
          </Route>
          <Route exact path='/offers'>
            <div className='app_page'>
              <Sidebar/>
              <Offers/>
            </div>
          </Route>
          <Route exact path='/snacks'>
            <div className='app_page'>
              <Sidebar/>
              <Snacks/>
            </div>
          </Route>
          <Route exact path='/contact'>
            <div className='app_page'>
              <Sidebar/>
              <Contactus/>
            </div>
          </Route>
          <Route exact path='/stayHome'>
            <div className='app_page'>
              <Sidebar/>
              <StayHome/>
            </div>
          </Route>
          <Route exact path='/bewarages'>
            <div className='app_page'>
              <Sidebar/>
              <Bewarages/>
            </div>
          </Route>
        
       
        </Switch>
        </Router>

      </div>
  )}
export default App;
